/**
 * Returns a promise where the data is the rep log collection
 *
 * @return {Promise<Response>}
 */
export function getRepLogs() {
  return fetch("/api/reps", {
    credentials: "same-origin",
    method: "GET",
  }).then(response => {
    // console.log("response", response);

    return response.json().then(data => {
      // console.log("data", data);

      return data.items;
    });
  });
}

/**
 * Returns a promise where the data is the rep log collection
 *
 * @return {Promise<Response>}
 */
export function createRepLog(rep) {
  return fetch("/api/reps", {
    credentials: "same-origin",
    method: "POST",
  }).then(response => {
    console.log("response", response);

    return response.json().then(data => {
      console.log("data", data);

      return data;
    });
  })
}

/**
 * Returns a promise where the data is the rep log collection
 *
 * @return {Promise<Response>}
 */
export function deleteRepLog(id) {
  return fetch(`/api/reps/${id}`, {
    credentials: "same-origin",
    method: "DELETE",
  }).then(response => {
    // console.log("response", response);
  });
}
