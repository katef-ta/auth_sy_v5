

import React, { useState, createContext, useContext } from "react";
import ReactDOM from "react-dom";

const UserContext = createContext();

is_authrized(role) {
  user = localStorage

  return user.role === role
}

function Component1() {
  const [user, setUser] = useState("Jesse Hall");

  return (
    <UserContext.Provider value={user}>
      {user.role === 1 ?
      <button></button> :''
      }
      <h1>{`Hello ${user}!`}</h1>
      <Component2 />
    </UserContext.Provider>
  );
}

function Component2() {
  return (
    <>
      <h1>Component 2</h1>
      <Component3 />
    </>
  );
}

function Component3() {
  return (
    <>
      <h1>Component 3</h1>
      <Component4 />
    </>
  );
}

function Component4() {
  return (
    <>
      <h1>Component 4</h1>
      <Component5 />
    </>
  );
}

function Component5() {
  const user = useContext(UserContext);

  return (
    <>
      <h1>Component 5</h1>
      <h2>{`Hello ${user} again!`}</h2>
    </>
  );
}

function Component6() {
  const [user, setUser] = useState("Foo Bar");

  return (
    <UserContext.Provider value={user}>
      <h1>Component 6</h1>
      <Component5 />
    </UserContext.Provider>
  );
}

function Component7() {
  const user = useContext(UserContext);

  return (
    <>
      <h1>Component 7</h1>
      <h2>{`Hello ${user} again!`}</h2>
    </>
  );
}

ReactDOM.render(<Component1 />, document.getElementById('root1'));
ReactDOM.render(<Component2 />, document.getElementById('root2'))
ReactDOM.render(<Component3 />, document.getElementById('root3'))
ReactDOM.render(<Component4 />, document.getElementById('root4'))
ReactDOM.render(<Component5 />, document.getElementById('root5'))
ReactDOM.render(<Component6 />, document.getElementById('root6'))
ReactDOM.render(<Component7 />, document.getElementById('root7'))



