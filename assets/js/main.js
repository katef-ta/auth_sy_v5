import React, { Component } from "react";
import ReactDom from "react-dom";
import PropTypes from "prop-types";
import { v4 as uuid } from "uuid";
import HTMLComment from "react-html-comment";
import { useFormik } from "formik";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { getRepLogs, createRepLog, deleteRepLog } from "./main_api";

const shouldShowHeart = true;

export class RepLogAppForm extends Component {
  constructor(props) {
    super(props);
    console.log("RepLogAppForm", props);

    this.state = {
      selectedItemId: "",
      quantityValue: 0,
      quantityInputError: "",
    };

    this.quantityInput = React.createRef();
    this.itemSelect = React.createRef();

    this.itemOptions = [
      { id: "cat", text: "Cat" },
      { id: "fat_cat", text: "Big Fat Cat" },
      { id: "laptop", text: "My Laptop" },
      { id: "coffee_cup", text: "Coffee Cup" },
    ];

    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    // this.handleChange = this.handleChange.bind(this);
    this.handleChangeItem = this.handleChangeItem.bind(this);
    this.handleChangeReps = this.handleChangeReps.bind(this);
  }
  handleChangeItem(event) {
    event.preventDefault();

    console.log("I love when a good form change item!");
    console.log(event.target);

    this.setState({
      selectedItemId: event.target.value,
    });
  }
  handleChangeReps(event) {
    event.preventDefault();

    console.log("I love when a good form change reps!");
    console.log(event.target);

    this.setState({
      quantityValue: event.target.value,
    });
  }
  handleFormSubmit(event) {
    event.preventDefault();

    const { onNewItemSubmit } = this.props;
    const { selectedItemId, quantityValue } = this.state;
    const quantityInput = this.quantityInput.current;
    const itemSelect = this.itemSelect.current;

    if (quantityValue <= 0) {
      // TODO - print some validation error!

      const msg = "Please enter a value greater than 0";
      this.setState({
        quantityInputError: msg,
      });
      toast.error(msg);

      console.log("error", msg, "state", this.state.quantityInputError);

      // don't submit, or clear the form
      return;
    }

    console.log("I love when a good form submits!");
    console.log("quantityInput", quantityInput, "itemSelect", itemSelect);
    console.log(
      event.target,
      event.target.elements.namedItem("item").value,
      event.target.elements.namedItem("reps").value
    );

    if (typeof onNewItemSubmit !== "undefined") {
      // onNewItemSubmit(
      //     event.target.elements.namedItem('item').value,
      //     event.target.elements.namedItem('reps').value,
      //     event
      // )
      onNewItemSubmit(
        itemSelect.options[selectedItemId].text,
        quantityValue,
        event
      );

      toast.success("New item have been saved");

      // reset form
      this.setState({
        selectedItemId: "",
        quantityValue: 0,
        quantityInputError: "",
      });
    }
  }
  render() {
    const { onNewItemSubmit } = this.props;
    const { selectedItemId, quantityValue, quantityInputError } = this.state;

    return (
      <form
        className="form-inline js-new-rep-log-form needs-validation"
        noValidate
        data-url="{{ path('rep_log_new') }}"
        onSubmit={this.handleFormSubmit}
      >
        <div className="form-group">
          <label
            className="sr-only control-label required"
            htmlFor="rep_log_item"
          >
            What did you lift?
          </label>
          <select
            id="rep_log_item"
            name="item"
            ref={this.itemSelect}
            required="required"
            className="form-control"
            value={selectedItemId}
            onChange={this.handleChangeItem}
          >
            <option value="">What did you lift?</option>
            {this.itemOptions.map(option => {
              return (
                <option value={option.id} key={option.id}>
                  {option.text}
                </option>
              );
            })}
          </select>
        </div>{" "}
        <div className="form-group">
          <label
            className="sr-only control-label required"
            htmlFor="rep_log_reps"
          >
            How many times?
          </label>
          <input
            type="number"
            id="rep_log_reps"
            name="reps"
            ref={this.quantityInput}
            required="required"
            placeholder="How many times?"
            className={`form-control ${quantityInputError ? "is-invalid" : ""}`}
            value={quantityValue}
            onChange={this.handleChangeReps}
            aria-describedby="repsHelpInline"
          />
          {quantityInputError && (
            <span id="repsHelpInline" className="invalid-feedback">
              {quantityInputError}
            </span>
          )}
        </div>{" "}
        <button type="submit" className="btn btn-primary">
          I Lifted it!
        </button>
      </form>
    );
  }
}

RepLogAppForm.propTypes = {
  onNewItemSubmit: PropTypes.func.isRequired,
  onAddRepLog: PropTypes.func,
};

function calculateTotalWeightLifted(repLogs) {
  let total = 0;
  for (let repLog of repLogs) {
    total += repLog.totalWeightLifted;
  }
  return total;
}
const calculateTotalWeightFancier = repLogs =>
  repLogs.reduce((total, log) => total + log.totalWeightLifted, 0);

export class RepLogAppList extends Component {
  constructor(props) {
    super(props);

    this.handleDeleteRepLog = this.handleDeleteRepLog.bind(this);
  }
  handleDeleteRepLog(event, repLogId) {
    event.preventDefault();

    const { onDeleteRepLog } = this.props;
    console.log("todo");

    if (typeof onDeleteRepLog === "function") {
      onDeleteRepLog(repLogId);
    }
  }
  render() {
    const { repLogs, highlightedRowId, onRowClick } = this.props;

    return (
      <>
        {repLogs.map(repLog => (
          <tr
            key={repLog.id}
            className={highlightedRowId === repLog.id ? "font-weight-bold" : ""}
            onClick={() => onRowClick(repLog, event)}
          >
            <td>{repLog.itemLabel}</td>
            <td>{repLog.reps}</td>
            <td className="text-right">{repLog.totalWeightLifted}</td>
            <td>
              <a
                href="#"
                onClick={event => handleDeleteClick(event, repLog.id)}
              >
                <span className="fa fa-trash"></span>
              </a>
            </td>
          </tr>
        ))}
      </>
    );
  }
}

RepLogAppList.propTypes = {
  highlightedRowId: PropTypes.any,
  onRowClick: PropTypes.func.isRequired,
  repLogs: PropTypes.array.isRequired,
  onDeleteRepLog: PropTypes.func.isRequired,
  // isLoaded: PropTypes.bool.isRequired,
};

export class RepLogAppTable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      highlightedRowId: null,
      repLogs: [
        {
          id: uuid(),
          reps: 25,
          itemLabel: "My Laptop",
          totalWeightLifted: 112.5,
        },
        {
          id: uuid(),
          reps: 10,
          itemLabel: "Big Fat Cat",
          totalWeightLifted: 180,
        },
        {
          id: uuid(),
          reps: 4,
          itemLabel: "Big Fat Cat",
          totalWeightLifted: 72,
        },
      ],
      numberOfHearts: 1,
      isLoaded: false,
      isSavingNewRepLog: false,
      successMessage: "",
    };

    this.handleRowClick = this.handleRowClick.bind(this);

    this.handleNewItemSubmit = this.handleNewItemSubmit.bind(this);

    console.log("RepLogAppTable", props, this.state);
  }
  componentDidMount() {
    getRepLogs().then(data => {
      this.setState(prevState => {
        return {
          repLogs: prevState.repLogs.concat(Object.values(data)),
          isLoaded: true,
        };
      });
      console.log(
        "componentDidMount",
        "data",
        data,
        "getRepLogs",
        this.state.repLogs
      );
    });
  }
  handleRowClick(repLog, event) {
    this.setState({ highlightedRowId: repLog.id });
    console.log("OMG an onClick!", this.state.highlightedRowId);
  }
  handleNewItemSubmit(itemLabel, reps, event) {
    console.log("TODO - handle this new data");
    console.log(itemLabel, reps);

    const newRep = {
      id: uuid(),
      itemLabel: itemLabel,
      reps: reps,
      totalWeightLifted: (Math.random() * 50).toFixed(1),
    };

    this.setState({
      isSavingNewRepLog: true,
    });

    createRepLog(newRep).then(repLog => {
      // const newRepLogs = [...this.state.repLogs, newRep];
      // this.setState({ repLogs: newRepLogs });
      this.setState(prevState => {
        const newRepLogs = [...prevState.repLogs, repLog];
        return {
          repLogs: newRepLogs,
          isSavingNewRepLog: false,
          successMessage: "Rep Log Saved!",
        };
      });
    });
  }
  onDeleteRepLog() {
    this.setState(prevState => {
      return { repLogs: this.state.repLogs.filter(repLog => repLog.id !== id) };
    });
  }
  render() {
    const {
      repLogs,
      highlightedRowId,
      isLoaded,
      isSavingNewRepLog,
      successMessage,
    } = this.state;

    return (
      <>
        {successMessage && (
          <div className="alert alert-success text-center">
            {successMessage}
          </div>
        )}

        <div className="table-responsive">
          <table className="table table-striped table-hover">
            <thead>
              <tr>
                <th>What</th>
                <th>How many times?</th>
                <th>Weight</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <tr className={isLoaded ? "d-none" : ""}>
                <td colSpan="4" className="text-center">
                  Loading...
                </td>
              </tr>
              <RepLogAppList
                repLogs={repLogs}
                highlightedRowId={highlightedRowId}
                onRowClick={this.handleRowClick}
                onDeleteRepLog={this.onDeleteRepLog}
                isLoaded={isLoaded}
              />
              {isSavingNewRepLog && (
                <tr>
                  <td
                    colSpan="4"
                    className="text-center"
                    style={{
                      opacity: 0.5,
                    }}
                  >
                    Lifting to the database ...
                  </td>
                </tr>
              )}
            </tbody>
            <tfoot>
              <tr>
                <td>&nbsp;</td>
                <th>Total</th>
                <th className="text-right">
                  {/*<HTMLComment text={calculateTotalWeightLifted(repLogs)} />*/}{" "}
                  {calculateTotalWeightFancier(repLogs)}
                </th>
                <td>&nbsp;</td>
              </tr>
            </tfoot>
          </table>

          <RepLogAppForm
            {...this.props}
            {...this.state}
            onNewItemSubmit={this.handleNewItemSubmit}
          />
        </div>
      </>
    );
  }
}

RepLogAppTable.propTypes = {
  //   // withHeart: PropTypes.bool,
  //   highlightedRowId: PropTypes.any,
  //   // onRowClick: PropTypes.func.isRequired,
  //   // onAddRepLog: PropTypes.func.isRequired,
  //   repLogs: PropTypes.array.isRequired,
  //   numberOfHearts: PropTypes.number.isRequired,
  //   // onHeartChange: PropTypes.func.isRequired,
  //   // onDeleteRepLog: PropTypes.func.isRequired,
  isSavingNewRepLog: PropTypes.bool,
};

export class RepLogApp extends Component {
  static defaultProps = {
    numberOfHearts: 2,
    onHeartChange: value => {
      this.setState(prevState => {
        console.log(
          "onHeartChange",
          "prevState",
          prevState.numberOfHearts,
          "numberOfHearts",
          value
        );

        return { numberOfHearts: value };
      });
    },
    withHeart: true
  };
  constructor(props) {
    super(props);

    this.state = {
      numberOfHearts: this.props.numberOfHearts,
    };

    console.log("RepLogApp", props);

    this.handleHeartChange = this.handleHeartChange.bind(this);
  }
  handleHeartChange(event) {
    const heartCount = event.target.value

    this.setState({
      numberOfHearts: heartCount,
    });

    const onHeartChange = this.props;

    if(typeof onHeartChange === "function") {
      onHeartChange(heartCount);
    }
  }
  render() {
    const { onHeartChange } = this.props;

    const { numberOfHearts } = this.state;

    let heart = "";

    if (this.props.withHeart) {
      heart = <span>{"❤️".repeat(numberOfHearts)}</span>;
    }

    return (
      <>
        <h3 className="h3">Lift Stuff! {heart}</h3>
        <input
          type="range"
          min="1"
          max="10"
          value={numberOfHearts}
          step="1"
          onChange={this.handleHeartChange}
        />
      </>
    );
  }
}

RepLogApp.propTypes = {
  numberOfHearts: PropTypes.number,
  onHeartChange: PropTypes.func,
  withHeart: PropTypes.bool
};

RepLogApp.defaultProps = {
  numberOfHearts: 1,
};

const el = React.createElement("h2", { className: "h2" }, "Lift History!");

if (document.getElementById("lift-stuff-app")) {
  ReactDom.render(el, document.getElementById("lift-stuff-app"));
}

console.log(el);

export class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div id="App">
        <header>
          <h1>FusionAuth Example: React</h1>
        </header>

        <div className="row">
          <div className="col-4">
            <RepLogApp withHeart={shouldShowHeart} />
          </div>
          <div className="col-4">
            <RepLogApp withHeart={false} />
          </div>
          <div className="col-4">
            <RepLogApp withHeart={shouldShowHeart} />
          </div>

          <div className="col-12">
            <RepLogAppTable />
          </div>
        </div>
      </div>
    );
  }
}

if (document.querySelector("#main")) {
  ReactDom.render(<App />, document.querySelector("#main"));
}

if (document.querySelector("#react-toast")) {
  ReactDom.render(
    <ToastContainer theme="dark" />,
    document.querySelector("#react-toast")
  );
}
