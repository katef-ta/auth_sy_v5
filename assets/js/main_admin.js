import React from 'react';
import ReactDOM from 'react-dom';
import {
  AdminGuesser,
  hydraDataProvider,
  hydraSchemaAnalyzer,
} from '@api-platform/admin';

const Admin = () => (
  <AdminGuesser
    // Use your custom data provider or resource schema analyzer
    dataProvider={hydraDataProvider({ entrypoint: 'https://demo.api-platform.com' })}
    schemaAnalyzer={hydraSchemaAnalyzer()}
  />
);

ReactDOM.render(<Admin />, document.getElementById('main-admin'));
