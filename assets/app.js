/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css'

// start the Stimulus application
import './bootstrap'

// loads the jquery package from node_modules
import $ from 'jquery'

// create global $ and jQuery variables
global.$ = global.jQuery = $

// import the function from greet.js (the .js extension is optional)
// ./ (or ../) means to look for a local file
import greet from './js/greet'

// or you can include specific pieces
require('bootstrap/js/dist/tooltip')
require('bootstrap/js/dist/popover')

// // require the JavaScript
// require('bootstrap-star-rating');
// // require 2 CSS files needed
// require('bootstrap-star-rating/css/star-rating.css');
// require('bootstrap-star-rating/themes/krajee-svg/theme.css');

jQuery(function ($) {
  if (typeof $.fn.popover === 'function') {
    $('[data-toggle="popover"]').popover()
  }

  $('#react-app').prepend('<h1>' + greet('jill') + '</h1>')
});
