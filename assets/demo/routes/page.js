import React from "react";
import { Route } from "react-router-dom";
import { List, Create, Update, Show } from "../components/page/";

export default [
  <Route path="/pages/create" component={Create} exact key="create" />,
  <Route path="/pages/edit/:id" component={Update} exact key="update" />,
  <Route path="/pages/show/:id" component={Show} exact key="show" />,
  <Route path="/pages/" component={List} exact strict key="list" />,
  <Route path="/pages/:page" component={List} exact strict key="page" />,
];
