<?php

namespace App\Utils;

class FileLoader
{
    public function get(string $path): ?string
    {
        if (file_exists($path)) {
            return file_get_contents($path);
        }
        return null;
    }
}
