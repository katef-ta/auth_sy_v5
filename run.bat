@ECHO ON

@REM call php80 bin/console d:m:m -n

call php80 bin\console d:s:u --dump-sql --force

call php80 bin\console c:c -e dev
call php80 bin\console c:c -e prod
call php80 bin\console c:c -e test

call yarn

call yarn encore dev
