@ECHO ON

call php81 bin\console cache:clear --env=prod --no-debug
call php81 bin\console cache:warmup --env=prod

set BOX_ALLOW_XDEBUG=1

call vendor\bin\box -v

call vendor\bin\box compile -vvv

if exist build (
    dir build
) else (
    echo "no build up"
)

:: https://medium.com/@cyrilgeorgespereira/create-a-compiled-php-console-application-and-deploy-it-with-homebrew-93876a8540fb
