#!/bin/sh

set -x

php bin/console d:s:u --dump-sql --force

php bin/console c:c -e dev

php bin/console c:c -e prod

chmod -R 777 var

set +x
