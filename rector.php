<?php

declare(strict_types=1);

use Rector\Core\Configuration\Option;
use Rector\Core\ValueObject\PhpVersion;
use Rector\Php74\Rector\Property\TypedPropertyRector;
use Rector\Set\ValueObject\LevelSetList;
use Rector\Set\ValueObject\SetList;
use Rector\Symfony\Set\SymfonySetList;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function (ContainerConfigurator $containerConfigurator): void {
  // here we can define, what sets of rules will be applied
  // tip: use "SetList" class to autocomplete sets
  $containerConfigurator->import(SetList::CODE_QUALITY);

  // get parameters
  $parameters = $containerConfigurator->parameters();
  // region Symfony Container
  $parameters->set(
    Option::SYMFONY_CONTAINER_XML_PATH_PARAMETER,
    __DIR__ . '/var/cache/dev/App_KernelDevDebugContainer.xml'
  );
  // endregion
  $parameters->set(Option::PATHS, [__DIR__ . '/src', __DIR__ . '/tests']);

  if (function_exists('SymfonySetList')):
    $containerConfigurator->import(SymfonySetList::SYMFONY_52);
    $containerConfigurator->import(SymfonySetList::SYMFONY_CODE_QUALITY);
    $containerConfigurator->import(
      SymfonySetList::SYMFONY_CONSTRUCTOR_INJECTION
    );
  endif;

  // is your PHP version different from the one you refactor to? [default: your PHP version], uses PHP_VERSION_ID format
  $parameters->set(Option::PHP_VERSION_FEATURES, PhpVersion::PHP_72);

  // Path to PHPStan with extensions, that PHPStan in Rector uses to determine types
  // $parameters->set(Option::PHPSTAN_FOR_RECTOR_PATH, getcwd() . '/phpstan-for-config.neon');

  // Define what rule sets will be applied
  $containerConfigurator->import(LevelSetList::UP_TO_PHP_74);
  // $containerConfigurator->import(LevelSetList::UP_TO_PHP_80);

  // get services (needed for register a single rule)
  $services = $containerConfigurator->services();

  // register a single rule
  $services->set(TypedPropertyRector::class);
};
