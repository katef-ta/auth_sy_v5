<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Conversation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteConversationCommand extends Command
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em, string $name = 'app:delete:conversation')
    {
        parent::__construct($name);

        $this->em = $em;
    }

    public function configure(): void
    {
        $this
            ->setDescription('Deletes a conversation')
            ->setDefinition([
                new InputArgument('name', InputArgument::REQUIRED, 'name')
            ])
            ->setHelp(
                <<<'EOT'
The <info>delete:conversation</info> command delete a conversation regarding an <info>name</info> argument
EOT
            );
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $name = $input->getArgument('name');
        $conversation = $this->em->getRepository(Conversation::class)->findOneBy([
            'name' => $name
        ]);

        if (!$conversation) {
            throw new \Exception('Conversation does not exist.');
        }

        $this->em->remove($conversation);
        $this->em->flush();

        return 0;
    }
}
