<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;

class UserCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;
    /**
     * @var UserPasswordHasherInterface
     */
    private UserPasswordHasherInterface $passwordHasher;
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    public function __construct(
        EntityManagerInterface $em,
        UserPasswordHasherInterface $passwordHasher,
        LoggerInterface $securityLogger
    ) {
        parent::__construct('app:user:add');
        $this->em = $em;
        $this->passwordHasher = $passwordHasher;
        $this->logger = $securityLogger;
    }

    protected function configure()
    {
        parent::configure();

        $this->setDescription('Create new user');

        $this->addArgument('username', InputArgument::REQUIRED, 'username');
        $this->addOption('admin', 'a', InputOption::VALUE_NONE, 'role admin');
        $this->addOption('member', 'm', InputOption::VALUE_NONE, 'role member');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $text = '';

        $username = 'admin';
        if (
            $input->hasArgument('username') &&
            $input->getArgument('username')
        ) {
            $username = $input->getArgument('username');
        }

        $user = new User();

        $user->setEmail("{$username}@gmail.com");
        $user->setPassword(
            $this->passwordHasher->hashPassword($user, $username)
        );

        $user->addRole(User::ROLE_USER);
        if ($input->hasOption('admin') && $input->getOption('admin')) {
            $user->addRole(User::ROLE_ADMIN);
        }
        if ($input->hasOption('member') && $input->getOption('member')) {
            $user->addRole(User::ROLE_USER);
        }

        $user->setCreatedAt(new \DateTime());
        $user->setUpdatedAt(new \DateTime());

        $this->em->persist($user);
        $this->em->flush();

        $this->logger->debug(sprintf("new user **%s** | id = %s", $user->getUserIdentifier(), $user->getId()), @compact('username') + ['date' => $user->getCreatedAt()->format(\DateTimeInterface::ISO8601)]);

        $io->writeln($text);

        $io->success("user \"{$username}\" saved");

        return self::SUCCESS;
    }
}
