<?php

namespace App\Command;

use App\Entity\Blog\Category;
use App\Entity\Blog\Comment;
use App\Entity\Blog\Post;
use App\Entity\Blog\Product;
use App\Entity\Blog\Page;
use App\Entity\Blog\Tag;
use App\Entity\Conversation;
use App\Entity\GroupConversation;
use App\Entity\User;
use App\Factory\Blog\CommentFactory;
use App\Factory\Blog\PostFactory;
use App\Factory\Blog\TagFactory;
use App\Factory\Blog\CategoryFactory;
use App\Factory\ConversationFactory;
use App\Factory\GroupConversationFactory;
use App\Factory\MessageFactory;
use App\Factory\Blog\PageFactory;
use App\Factory\Blog\ProductFactory;
use App\Factory\UserFactory;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;
use Symfony\Component\Stopwatch\Stopwatch;

# php bin/console a:f:g -vvv
class GenerateFactoryCommand extends Command
{
    use LockableTrait;

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;
    /**
     * @var UserPasswordHasherInterface
     */
    private UserPasswordHasherInterface $passwordHasher;
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    public function __construct(
        EntityManagerInterface $em,
        UserPasswordHasherInterface $passwordHasher,
        LoggerInterface $securityLogger
    ) {
        parent::__construct('app:factory:generate');
        $this->em = $em;
        $this->passwordHasher = $passwordHasher;
        $this->logger = $securityLogger;
    }

    protected function configure()
    {
        parent::configure();

        $this->setDescription('Create Factory entity');

        $this->addArgument('model', InputArgument::OPTIONAL, 'model');
        $this->addOption('model', 'm', InputOption::VALUE_NONE, 'model');
        // $this->addOption('member', 'm', InputOption::VALUE_NONE, 'role member');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $text = '';

        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return self::SUCCESS;
        }

        // If you prefer to wait until the lock is released, use this:
        // $this->lock(null, true);

        // ...

        // if not released explicitly, Symfony releases the lock
        // automatically when the execution of the command ends
        $this->release();

        $model = null;
        if ($input->hasArgument('model') && $input->getArgument('model')) {
            $model = $input->getArgument('model');
        }
        if ($input->hasOption('model') && $input->getOption('model')) {
            $model = $input->getOption('model');
        }

        $io->writeln("start");

        $stopwatch = new Stopwatch();
        $eventwatch = [];

        if ($model) {
            if ($model == 'user') {
                UserFactory::createOne();
            }
        } else {
            $stopwatch->start('users');
            $user = UserFactory::createOne();
            $admin = UserFactory::createOne();
            $users = array_merge([$user], UserFactory::createMany(3));
            $eventwatch['users'] = $stopwatch->stop('users');

            $io->success("Users saved | " . $eventwatch['users']);

            $stopwatch->start('conversations');
            $conversation = ConversationFactory::createOne([]);
            GroupConversationFactory::createOne([
                'users' => array_map(function ($user) {
                    return method_exists($user, 'object') ? $user->object() : $user;
                }, $users),
                'admin' => $admin->object(),
            ]);
            $messages = MessageFactory::createMany(8, ['conversation' => $conversation->object(), 'author' => $user]);
            $eventwatch['conversations'] = $stopwatch->stop('conversations');

            $io->success("Conversations saved | " . $eventwatch['conversations']);

            $stopwatch->start('blog');
            $category = CategoryFactory::createOne();
            $post = PostFactory::createOne(['category' => $category->object(), 'author' => $user->object()]);
            PageFactory::createOne();
            ProductFactory::createOne(['category' => $category->object()]);
            CommentFactory::createMany(4, [
                'post' => $post->object(),
                'author' => $user->object(),
            ]);
            TagFactory::createMany(4, ['post' => $post->object()]);
            $eventwatch['blog'] = $stopwatch->stop('blog');

            $io->success("Blog saved | " . $eventwatch['blog']);
        }
        $io->writeln("end");

        if (function_exists('dump')):
            dump($eventwatch);
        endif;

        return self::SUCCESS;
    }
}
