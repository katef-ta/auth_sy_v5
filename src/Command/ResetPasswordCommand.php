<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;
use Symfony\Component\Console\Helper\ProgressBar;

class ResetPasswordCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;
    /**
     * @var UserPasswordHasherInterface
     */
    private UserPasswordHasherInterface $passwordHasher;
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    public function __construct(
        EntityManagerInterface      $em,
        UserPasswordHasherInterface $passwordHasher,
        LoggerInterface             $securityLogger
    )
    {
        parent::__construct('app:user:reset:password');
        $this->em = $em;
        $this->passwordHasher = $passwordHasher;
        $this->logger = $securityLogger;
    }

    protected function configure()
    {
        parent::configure();

        $this->setDescription('Rest password users');

        $this->addOption('username', 'u', InputOption::VALUE_OPTIONAL, 'username');
        $this->addOption('admin', 'a', InputOption::VALUE_NONE, 'role admin');
        $this->addOption('member', 'm', InputOption::VALUE_NONE, 'role member');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $text = '';

        /** @var QueryBuilder $qb */
        $qb = $this->em->createQueryBuilder();
        $qb->select('user')
            ->from(User::class, 'user')
            ->where('user.id > 0')
            ->orderBy('id', 'ASC');

        if ($input->hasOption('admin') && $input->getOption('admin')) {
            $qb->andWhere("JSON_SEARCH(user.roles, 'all', 'ROLE_ADMIN')");
        }
        if ($input->hasOption('member') && $input->getOption('member')) {
            $qb->andWhere("JSON_SEARCH(user.roles, 'all', 'ROLE_USER')");
        }

        $username = null;

        /** @var User[] $users */
        $users = $qb->getQuery()->getResult(AbstractQuery::HYDRATE_OBJECT);

        $progressBar = new ProgressBar($output, count($users));
        $text .= "count users = " . count($users) . "\n";

        $progressBar->start();
        foreach ($users as $index => $user) {
            $username = $user->getUserIdentifier();
            $password = "admin123";

            $text .= sprintf(
                "%03d | %-32s | %-32s | %-32s | %s\n",
                $user->getId(),
                $user->getUsername(),
                $user->getUserIdentifier(),
                $password,
                json_encode($user->getroles()),
            );

            $user->setPassword($this->passwordHasher->hashPassword($user, $password));

            $user->setUpdatedAt(new \DateTime());

            $this->fixData($user);

            $this->em->persist($user);
            $this->em->flush();

            $this->logger->debug(
                sprintf(
                    "new user **%s** | id = %s",
                    $user->getUserIdentifier(),
                    $user->getId()
                ),
                @compact('username') + [
                    'date' => $user->getCreatedAt()->format(\DateTimeInterface::ISO8601),
                ]
            );

            $progressBar->advance();
        }
        $this->em->flush();

        $progressBar->finish();

        $io->newLine(2);

        $io->writeln($text);

        $io->success("user \"{$username}\" saved");

        return self::SUCCESS;
    }

    /**
     * @param User $user
     * @return void
     */
    protected function fixData(User $user): void
    {
        $faker = \Faker\Factory::create('fr_FR');

        $user->setIsVerified(true);
        if(empty($user->getPhone())) {
            $user->setPhone($faker->phoneNumber());
        }
    }
}
