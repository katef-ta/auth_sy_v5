<?php

namespace App\MessageHandler;

use App\Entity\Blog\Comment;
use App\Entity\User;
use App\Message\CommentNotification;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\Util\StringUtil;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\String\Slugger\AsciiSlugger;

#[AsMessageHandler]
class CommentNotificationHandler implements MessageHandlerInterface
{
    private ParameterBagInterface $parameterBag;
    private EntityManagerInterface $em;
    public function __construct(ParameterBagInterface $parameterBag, EntityManagerInterface $em)
    {
        $this->parameterBag = $parameterBag;
        $this->em = $em;
    }

    public function __invoke(CommentNotification $message)
    {
        $users = $this->em->getRepository(User::class)->findAll();

        // ... do some work - like sending an Comment message!
        $path = $this->parameterBag->get('kernel.project_dir');
        file_put_contents(
            sprintf("%s/%s-%s.json", $path, (new AsciiSlugger())->slug(Comment::class), time()),
            $message->__toString()
        );

        foreach ($users as $user) {
            file_put_contents(
                sprintf("%s/%s-%s-%s.json", $path, (new AsciiSlugger())->slug(Comment::class), time(), 'uid-'.$user->getId()),
                $message->__toString()
            );
        }
    }
}
