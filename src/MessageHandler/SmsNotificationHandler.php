<?php

namespace App\MessageHandler;

use App\Entity\Blog\Post;
use App\Message\SmsNotification;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\String\Slugger\AsciiSlugger;

#[AsMessageHandler]
class SmsNotificationHandler implements MessageHandlerInterface
{
    private ParameterBagInterface $parameterBag;
    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
    }

    public function __invoke(SmsNotification $message)
    {
        // ... do some work - like sending an SMS message!
        $path = $this->parameterBag->get('kernel.project_dir') . '/var/notifications';
        file_put_contents(
            sprintf("%s/%s-%s.json", $path, (new AsciiSlugger())->slug(Post::class)->lower(), time()),
            $message->__toString()
        );
    }
}
