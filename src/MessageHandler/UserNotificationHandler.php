<?php

namespace App\MessageHandler;

use App\Entity\User;
use App\Message\UserNotification;
use Enqueue\Client\ProducerInterface;
use Enqueue\Client\Message as EnqueueMessage;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\String\Slugger\AsciiSlugger;

#[AsMessageHandler]
class UserNotificationHandler implements MessageHandlerInterface
{
    private ParameterBagInterface $parameterBag;
    private ProducerInterface $producer;
    public function __construct(ParameterBagInterface $parameterBag, ProducerInterface $producer)
    {
        $this->parameterBag = $parameterBag;
        $this->producer = $producer;
    }

    public function __invoke(UserNotification $message)
    {
        // ... do some work - like sending an User message!
        $path = $this->parameterBag->get('kernel.project_dir') . '/var/notifications';
        file_put_contents(
            sprintf("%s/%s-%s.json", $path, (new AsciiSlugger())->slug(User::class)->lower(), time()),
            $message->__toString()
        );

        $this->producer->sendEvent('user-topic', new EnqueueMessage($message->__toString(), [], []));
    }
}
