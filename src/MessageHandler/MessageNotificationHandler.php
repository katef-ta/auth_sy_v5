<?php

namespace App\MessageHandler;

use App\Entity\Message;
use Enqueue\Client\Message as EnqueueMessage;
use App\Message\MessageNotification;
use Enqueue\Client\ProducerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\String\Slugger\AsciiSlugger;

#[AsMessageHandler]
class MessageNotificationHandler implements MessageHandlerInterface
{
    private ParameterBagInterface $parameterBag;
    private ProducerInterface $producer;
    public function __construct(ParameterBagInterface $parameterBag, ProducerInterface $producer)
    {
        $this->parameterBag = $parameterBag;
        $this->producer = $producer;
    }

    public function __invoke(MessageNotification $message)
    {
        // ... do some work - like sending an Message message!
        $path = $this->parameterBag->get('kernel.project_dir') . '/var/notifications';
        file_put_contents(
            sprintf("%s/%s-%s.json", $path, (new AsciiSlugger())->slug(Message::class)->lower(), time()),
            $message->__toString()
        );

        $this->producer->sendEvent('message-topic', new EnqueueMessage($message->__toString(), [], []));
        $this->producer->sendCommand("echo \"{$message->__toString()}\";", new EnqueueMessage($message->__toString(), [], []));
    }
}
