<?php

namespace App\Factory\Blog;

use App\Entity\Blog\Comment;
use App\Message\CommentNotification;
use App\Repository\Blog\CommentRepository;
use Symfony\Component\Messenger\MessageBusInterface;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<Comment>
 *
 * @method static Comment|Proxy createOne(array $attributes = [])
 * @method static Comment[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Comment|Proxy find(object|array|mixed $criteria)
 * @method static Comment|Proxy findOrCreate(array $attributes)
 * @method static Comment|Proxy first(string $sortedField = 'id')
 * @method static Comment|Proxy last(string $sortedField = 'id')
 * @method static Comment|Proxy random(array $attributes = [])
 * @method static Comment|Proxy randomOrCreate(array $attributes = [])
 * @method static Comment[]|Proxy[] all()
 * @method static Comment[]|Proxy[] findBy(array $attributes)
 * @method static Comment[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Comment[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static CommentRepository|RepositoryProxy repository()
 * @method Comment|Proxy create(array|callable $attributes = [])
 */
final class CommentFactory extends ModelFactory
{
    private MessageBusInterface $bus;
    public function __construct(MessageBusInterface $bus)
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
            $this->bus = $bus;
    }

    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'subject' => self::faker()->text(),
            'createdAt' => self::faker()->dateTime(), // TODO add DATETIME ORM type manually
            'updatedAt' => self::faker()->dateTime(), // TODO add DATETIME ORM type manually
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(Comment $comment): void {})
            ->afterPersist(function (Proxy $proxy, array $attributes) {
                $this->bus->dispatch(new CommentNotification($proxy->object()->getId(), 'New comment'));

                $this->bus->dispatch(new CommentNotification($proxy->object()->getId(), 'New comment double'), []);
            })
        ;
    }

    protected static function getClass(): string
    {
        return Comment::class;
    }
}
