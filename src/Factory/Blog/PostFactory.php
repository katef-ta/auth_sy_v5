<?php

namespace App\Factory\Blog;

use App\Entity\Blog\Post;
use App\Message\PostNotification;
use App\Repository\Blog\PostRepository;
use Symfony\Component\Messenger\MessageBusInterface;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<Post>
 *
 * @method static Post|Proxy createOne(array $attributes = [])
 * @method static Post[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Post|Proxy find(object|array|mixed $criteria)
 * @method static Post|Proxy findOrCreate(array $attributes)
 * @method static Post|Proxy first(string $sortedField = 'id')
 * @method static Post|Proxy last(string $sortedField = 'id')
 * @method static Post|Proxy random(array $attributes = [])
 * @method static Post|Proxy randomOrCreate(array $attributes = [])
 * @method static Post[]|Proxy[] all()
 * @method static Post[]|Proxy[] findBy(array $attributes)
 * @method static Post[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Post[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static PostRepository|RepositoryProxy repository()
 * @method Post|Proxy create(array|callable $attributes = [])
 */
final class PostFactory extends ModelFactory
{
    private MessageBusInterface $bus;
    public function __construct(MessageBusInterface $bus)
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
            $this->bus = $bus;
    }

    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'title' => self::faker()->text(),
            'slug' => self::faker()->text(),
            'summary' => self::faker()->text(),
            'createdAt' => self::faker()->dateTime(), // TODO add DATETIME ORM type manually
            'updatedAt' => self::faker()->dateTime(), // TODO add DATETIME ORM type manually
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(Post $post): void {})
            ->afterPersist(function (Proxy $proxy, array $attributes) {
                $this->bus->dispatch(new PostNotification($proxy->object()->getId(), 'New post'));

                $this->bus->dispatch(new PostNotification($proxy->object()->getId(), 'New post double'), []);
            })
        ;
    }

    protected static function getClass(): string
    {
        return Post::class;
    }
}
