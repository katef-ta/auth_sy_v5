<?php

namespace App\Factory;

use App\Entity\Message;
use App\Message\MessageNotification;
use App\Repository\MessageRepository;
use Symfony\Component\Messenger\MessageBusInterface;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<Message>
 *
 * @method static Message|Proxy createOne(array $attributes = [])
 * @method static Message[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Message|Proxy find(object|array|mixed $criteria)
 * @method static Message|Proxy findOrCreate(array $attributes)
 * @method static Message|Proxy first(string $sortedField = 'id')
 * @method static Message|Proxy last(string $sortedField = 'id')
 * @method static Message|Proxy random(array $attributes = [])
 * @method static Message|Proxy randomOrCreate(array $attributes = [])
 * @method static Message[]|Proxy[] all()
 * @method static Message[]|Proxy[] findBy(array $attributes)
 * @method static Message[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Message[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static MessageRepository|RepositoryProxy repository()
 * @method Message|Proxy create(array|callable $attributes = [])
 */
final class MessageFactory extends ModelFactory
{
    private MessageBusInterface $bus;
    public function __construct(MessageBusInterface $bus)
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
        $this->bus = $bus;
    }

    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'content' => self::faker()->text(),
            'seen' => self::faker()->boolean(),
            'createdAt' => self::faker()->dateTime(), // TODO add DATETIME ORM type manually
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            ->afterInstantiate(function (Message $message, array $attributes): void {
                if(function_exists('dump')):
                    dump(@compact('message', 'attributes'));
                endif;
            })
            ->afterPersist(function (Proxy $proxy, array $attributes) {
                $this->bus->dispatch(new MessageNotification($proxy->object()->getId(), 'New message'));

                $this->bus->dispatch(new MessageNotification($proxy->object()->getId(), 'New message double'));
            });
    }

    protected static function getClass(): string
    {
        return Message::class;
    }
}
