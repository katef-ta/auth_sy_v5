<?php

namespace App\Factory;

use App\Entity\User;
use App\Message\UserNotification;
use App\Repository\UserRepository;
use Symfony\Component\Messenger\MessageBusInterface;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @extends ModelFactory<User>
 *
 * @method static User|Proxy createOne(array $attributes = [])
 * @method static User[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static User|Proxy find(object|array|mixed $criteria)
 * @method static User|Proxy findOrCreate(array $attributes)
 * @method static User|Proxy first(string $sortedField = 'id')
 * @method static User|Proxy last(string $sortedField = 'id')
 * @method static User|Proxy random(array $attributes = [])
 * @method static User|Proxy randomOrCreate(array $attributes = [])
 * @method static User[]|Proxy[] all()
 * @method static User[]|Proxy[] findBy(array $attributes)
 * @method static User[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static User[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static UserRepository|RepositoryProxy repository()
 * @method User|Proxy create(array|callable $attributes = [])
 */
final class UserFactory extends ModelFactory
{
    private MessageBusInterface $bus;
    private UserPasswordHasherInterface $passwordHasher;
    public function __construct(MessageBusInterface $bus, UserPasswordHasherInterface $passwordHasher)
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
        $this->bus = $bus;
        $this->passwordHasher = $passwordHasher;
    }

    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'username' => self::faker()
                ->unique()
                ->userName(),
            'email' => self::faker()
                ->unique()
                ->email(),
            'phone' => self::faker()
                ->unique()
                ->phoneNumber(),
            'password' => self::faker()->password(12, 12),
            'roles' => [self::faker()->boolean() ? User::ROLE_ADMIN : User::ROLE_USER],
            'isVerified' => self::faker()->boolean(),
            'createdAt' => self::faker()->dateTime(), // TODO add DATETIME ORM type manually
            'updatedAt' => self::faker()->dateTime(), // TODO add DATETIME ORM type manually
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this->beforeInstantiate(function (array $attributes) {
            if (self::faker()->boolean()) {
                $attributes['activationToken'] = self::faker()->linuxPlatformToken();
            }

            return $attributes;
        })
            ->afterInstantiate(function (User $user, array $attributes): void {
                if(function_exists('dump')):
                    dump(@compact('user', 'attributes'));
                endif;
                $user->setPassword($this->passwordHasher->hashPassword($user, $user->getPassword()));
            })
            ->afterPersist(function (Proxy $proxy, array $attributes) {
                $this->bus->dispatch(new UserNotification($proxy->object()->getId(), 'New user'));
            });
    }

    protected static function getClass(): string
    {
        return User::class;
    }
}
