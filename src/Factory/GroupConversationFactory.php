<?php

namespace App\Factory;

use App\Entity\GroupConversation;
use App\Repository\GroupConversationRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<GroupConversation>
 *
 * @method static GroupConversation|Proxy createOne(array $attributes = [])
 * @method static GroupConversation[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static GroupConversation|Proxy find(object|array|mixed $criteria)
 * @method static GroupConversation|Proxy findOrCreate(array $attributes)
 * @method static GroupConversation|Proxy first(string $sortedField = 'id')
 * @method static GroupConversation|Proxy last(string $sortedField = 'id')
 * @method static GroupConversation|Proxy random(array $attributes = [])
 * @method static GroupConversation|Proxy randomOrCreate(array $attributes = [])
 * @method static GroupConversation[]|Proxy[] all()
 * @method static GroupConversation[]|Proxy[] findBy(array $attributes)
 * @method static GroupConversation[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static GroupConversation[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static GroupConversationRepository|RepositoryProxy repository()
 * @method GroupConversation|Proxy create(array|callable $attributes = [])
 */
final class GroupConversationFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'name' => self::faker()->text(),
            'created' => self::faker()->dateTime(), // TODO add DATETIMETZ ORM type manually
            'updated' => self::faker()->dateTime(), // TODO add DATETIMETZ ORM type manually
            'private' => self::faker()->boolean(),
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(GroupConversation $groupConversation): void {})
        ;
    }

    protected static function getClass(): string
    {
        return GroupConversation::class;
    }
}
