<?php

namespace App\Message;

class MessageNotification
{
    protected string $message_id;
    protected string $content;

    public function __construct(string $message_id, string $content)
    {
        $this->message_id = $message_id;
        $this->content = $content;
    }

    public function getMessageId()
    {
        return $this->message_id;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function __toString()
    {
        return sprintf("%s {message_id:%s, content:%s}\n", __CLASS__, $this->message_id, $this->content);
    }

}
