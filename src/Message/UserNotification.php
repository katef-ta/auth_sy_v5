<?php

namespace App\Message;

class UserNotification
{
    protected string $user_id;
    protected string $content;

    public function __construct(string $user_id, string $content)
    {
        $this->user_id = $user_id;
        $this->content = $content;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function __toString()
    {
        return sprintf("%s {user_id:%s, content:%s}\n", __CLASS__, $this->user_id, $this->content);
    }

}
