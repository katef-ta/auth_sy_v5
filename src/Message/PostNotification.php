<?php

namespace App\Message;

class PostNotification
{
    protected string $post_id;
    protected string $content;

    public function __construct(string $post_id, string $content)
    {
        $this->post_id = $post_id;
        $this->content = $content;
    }

    public function getPostId()
    {
        return $this->post_id;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function __toString()
    {
        return sprintf("%s {post_id:%s, content:%s}\n", __CLASS__, $this->post_id, $this->content);
    }

}
