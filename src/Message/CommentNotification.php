<?php

namespace App\Message;

class CommentNotification
{
    protected string $comment_id;
    protected string $content;

    public function __construct(string $comment_id, string $content)
    {
        $this->comment_id = $comment_id;
        $this->content = $content;
    }

    public function getCommentId()
    {
        return $this->comment_id;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function __toString()
    {
        return sprintf("%s {comment_id:%s, content:%s}\n", __CLASS__, $this->comment_id, $this->content);
    }

}
