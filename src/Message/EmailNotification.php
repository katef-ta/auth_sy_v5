<?php

namespace App\Message;

class EmailNotification extends SmsNotification
{
    public function __toString()
    {
        return sprintf("%s {%s, %s}\n", __CLASS__, $this->phonenumber, $this->content);
    }
}
