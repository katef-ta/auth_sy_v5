<?php

namespace App\Message;

class SmsNotification
{
    protected string $phonenumber;
    protected string $content;
    protected string $model;

    public function __construct(string $phonenumber, string $content, string $model)
    {
        $this->phonenumber = $phonenumber;
        $this->content = $content;
        $this->model = $model;
    }

    /**
     * Get the value of content
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * Get the value of phonenumber
     */
    public function getPhonenumber(): string
    {
        return $this->phonenumber;
    }

    /**
     * Get the value of model
     */
    public function getModel(): string
    {
        return $this->model;
    }

    public function __toString()
    {
        return sprintf("%s {phonenumber:%s, content:%s}\n", __CLASS__, $this->phonenumber, $this->content);
    }
}
