<?php

namespace App\EventListener;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Honeybadger\Honeybadger;
use Psr\Log\LoggerInterface;

class ExceptionListener
{
  /** @var Honeybadger */
  private Honeybadger $honeybadger;
  /** @var LoggerInterface */
  private LoggerInterface $logger;

  public function __construct(
    ParameterBagInterface $parameterBag,
    LoggerInterface $securityLogger
  ) {
    $this->honeybadger = Honeybadger::new([
      'api_key' => $parameterBag->get('honeybadger_api_key'),
    ]);
    $this->logger = $securityLogger;
  }

  public function onKernelException(ExceptionEvent $event)
  {
    // You get the exception object from the received event
    $exception = $event->getThrowable();
    $message = sprintf(
      'My Error says: %s with code: %s',
      $exception->getMessage(),
      $exception->getCode()
    );

    // Customize your response object to display the exception details
    $response = new Response();
    $response->setContent($message);

    // HttpExceptionInterface is a special type of exception that
    // holds status code and header details
    if ($exception instanceof HttpExceptionInterface) {
      $response->setStatusCode($exception->getStatusCode());
      $response->headers->replace($exception->getHeaders());
    } else {
      $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    // sends the modified response object to the event
    // $event->setResponse($response);

    try {
      $this->honeybadger->notify($exception);
    } catch (\Exception $e) {
      $this->logger->error("exception listener", @compact('message'));
    }
  }
}
