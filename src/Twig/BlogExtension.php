<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class BlogExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('blog_name', fn(string $value): ?string => $this->blogName($value)),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('blog_name', fn(string $value): ?string => $this->blogName($value))
        ];
    }

    public function blogName(string $value): ?string
    {
        if($value) {
            return $value;
        }
        return null;
    }
}
