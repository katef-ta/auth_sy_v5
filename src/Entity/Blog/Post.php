<?php

namespace App\Entity\Blog;

use App\Entity\User;
use App\Repository\Blog\PostRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=PostRepository::class)
 * @ORM\Table(name="`post`")
 * @UniqueEntity(fields={"slug"}, errorPath="title", message="post.slug_unique")
 */
class Post
{
  use TimestampableEntity;
  /**
   * Use constants to define configuration options that rarely change instead
   * of specifying them under parameters section in config/services.yaml file.
   *
   * See https://symfony.com/doc/current/best_practices.html#use-constants-to-define-options-that-rarely-change
   */
  public const NUM_ITEMS = 10;

  /**
   * @var int
   * @ORM\Id
   * @ORM\GeneratedValue
   * @ORM\Column(type="integer")
   */
  private int $id;

  /**
   * @var string
   * @ORM\Column(type="string", length=200, unique=true)
   * @Assert\NotBlank
   */
  private string $title;

  /**
   * @var string
   *
   * @ORM\Column(type="string")
   */
  private string $slug;

  /**
   * @var string
   * @ORM\Column(type="text", nullable=true)
   * @Assert\NotBlank(message="post.blank_content")
   * @Assert\Length(min=10, minMessage="post.too_short_content")
   */
  private string $content;

  /**
   * @var string
   *
   * @ORM\Column(type="string")
   * @Assert\NotBlank(message="post.blank_summary")
   * @Assert\Length(max=255)
   */
  private string $summary;

  /**
   * @var \DateTime|\DateTimeImmutable
   *
   * @ORM\Column(type="datetime", nullable=true)
   */
  private \DateTimeInterface $publishedAt;

  /**
   * @var string
   * @ORM\Column(type="string", length=100, nullable=true)
   */
  private string $status;

  /**
   * @var Category
   * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="posts")
   */
  private Category $category;

  /**
   * @var User
   * @ORM\ManyToOne(targetEntity=User::class, inversedBy="posts")
   */
  private User $author;

  /**
   * @var Comment[]|ArrayCollection
   *
   * @ORM\OneToMany(
   *      targetEntity="Comment",
   *      mappedBy="post",
   *      orphanRemoval=true,
   *      cascade={"persist"}
   * )
   * @ORM\OrderBy({"publishedAt": "DESC"})
   */
  private $comments;

  /**
   * @var Tag[]|ArrayCollection
   *
   * @ORM\ManyToMany(targetEntity=Tag::class, cascade={"persist"})
   * @ORM\JoinTable(name="`post_tag`")
   * @ORM\OrderBy({"name": "ASC"})
   * @Assert\Count(max="4", maxMessage="post.too_many_tags")
   */
  private $tags;

  public function __construct()
  {
    $this->publishedAt = new \DateTime();
    $this->comments = new ArrayCollection();
    $this->tags = new ArrayCollection();
  }

  public function getId(): ?int
  {
    return $this->id;
  }

  /**
   * Get the value of title
   *
   * @return  string
   */
  public function getTitle(): ?string
  {
    return $this->title;
  }

  /**
   * Set the value of title
   *
   * @param  string  $title
   *
   * @return  self
   */
  public function setTitle(string $title)
  {
    $this->title = $title;

    return $this;
  }

  public function getSlug(): ?string
  {
    return $this->slug;
  }

  public function setSlug(string $slug): void
  {
    $this->slug = $slug;
  }

  /**
   * Get the value of content
   *
   * @return  string
   */
  public function getContent()
  {
    return $this->content;
  }

  /**
   * Set the value of content
   *
   * @param  string  $content
   *
   * @return  self
   */
  public function setContent(string $content)
  {
    $this->content = $content;

    return $this;
  }

  public function getPublishedAt(): \DateTimeInterface
  {
    return $this->publishedAt;
  }

  public function setPublishedAt(\DateTimeInterface $publishedAt): self
  {
    $this->publishedAt = $publishedAt;
    return $this;
  }

  /**
   * Get the value of status
   *
   * @return  string
   */
  public function getStatus()
  {
    return $this->status;
  }

  /**
   * Set the value of status
   *
   * @param  string  $status
   *
   * @return  self
   */
  public function setStatus(string $status)
  {
    $this->status = $status;

    return $this;
  }

  /**
   * Get the value of category
   *
   * @return  Category
   */
  public function getCategory()
  {
    return $this->category;
  }

  /**
   * Set the value of category
   *
   * @param  Category  $category
   *
   * @return  self
   */
  public function setCategory(?Category $category)
  {
    $this->category = $category;

    return $this;
  }

  /**
   * Get the value of author
   *
   * @return  User
   */
  public function getAuthor()
  {
    return $this->author;
  }

  /**
   * Set the value of author
   *
   * @param  User  $author
   *
   * @return  self
   */
  public function setAuthor(User $author)
  {
    $this->author = $author;

    return $this;
  }

  public function getComments(): Collection
  {
    return $this->comments;
  }

  public function addComment(Comment $comment): void
  {
    $comment->setPost($this);
    if (!$this->comments->contains($comment)) {
      $this->comments->add($comment);
    }
  }

  public function removeComment(Comment $comment): void
  {
    $this->comments->removeElement($comment);
  }

  public function getSummary(): ?string
  {
    return $this->summary;
  }

  public function setSummary(string $summary): void
  {
    $this->summary = $summary;
  }

  public function addTag(Tag ...$tags): void
  {
    foreach ($tags as $tag) {
      if (!$this->tags->contains($tag)) {
        $this->tags->add($tag);
      }
    }
  }

  public function removeTag(Tag $tag): void
  {
    $this->tags->removeElement($tag);
  }

  public function getTags(): Collection
  {
    return $this->tags;
  }
}
