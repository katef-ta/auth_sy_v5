<?php

namespace App\Entity\Blog;

use App\Entity\User;
use App\Repository\Blog\PageRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=PageRepository::class)
 * @ORM\Table(name="`page`")
 * @UniqueEntity(fields={"slug"}, errorPath="title", message="page.slug_unique")
 */
class Page
{
  use TimestampableEntity;

  /**
   * @var int
   * @ORM\Id
   * @ORM\GeneratedValue
   * @ORM\Column(type="integer")
   */
  private int $id;

  /**
   * @var string
   * @ORM\Column(type="string", length=200, unique=true)
   * @Assert\NotBlank
   */
  private string $title;

  /**
   * @var string
   *
   * @ORM\Column(type="string")
   */
  private string $slug;

  public function __construct()
  {

  }

  public function getId(): ?int
  {
    return $this->id;
  }

  /**
   * Get the value of title
   *
   * @return  string
   */
  public function getTitle(): ?string
  {
    return $this->title;
  }

  /**
   * Set the value of title
   *
   * @param  string  $title
   *
   * @return  self
   */
  public function setTitle(string $title)
  {
    $this->title = $title;

    return $this;
  }

  public function getSlug(): ?string
  {
    return $this->slug;
  }

  public function setSlug(string $slug): void
  {
    $this->slug = $slug;
  }

}
