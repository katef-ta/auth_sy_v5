<?php

namespace App\Repository\Signature;

use App\Entity\User;

interface UserRepositoryInterface
{
    // function find($id): User;
    // function findAll(): array;
    function add(User $user): void;
}
