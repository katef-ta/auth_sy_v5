<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture implements FixtureGroupInterface
{
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }
    /**
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $admin1 = new User();
        $admin1->setEmail('admin@gmail.com');
        $admin1->setPassword($this->passwordHasher->hashPassword($admin1, 'admin'));
        $admin1->setRoles(['ROLE_ADMIN']);
        $admin2 = new User();
        $admin2->setEmail('admin2@gmail.com');
        $admin2->setPassword($this->passwordHasher->hashPassword($admin2, 'admin'));
        $admin2->setRoles(['ROLE_ADMIN']);
        $manager->persist($admin1);
        $manager->persist($admin2);
        for ($i = 1; $i <= 5; $i++) {
            $user = new User();
            $user->setEmail("user{$i}@gmail.com");
            $user->setPassword($this->passwordHasher->hashPassword($user, 'user'));
            $manager->persist($user);
        }

        // create 20 users! Bam!
        for ($i = 0; $i < 20; $i++) {
            $user = new User();
            $password = $this->passwordHasher->hashPassword($user, random_bytes(10));
            $user->setUsername('Florian' . random_int(0, 1000));
            $user->setEMail('test' . random_int(0, 1000) . '@test1.fr');
            $user->setPassword($password);
            $user->setRoles(['ROLE_USER']);
            // $user->setStatus(1);
            $user->setCreatedAt(new \DateTime());
            $user->setUpdatedAt(new \DateTime());

            $manager->persist($user);
            $manager->flush();
        }
    }

    public static function getGroups(): array
    {
        return [];
    }
}
