<?php

namespace App\Controller\Admin;

use App\Entity\Blog\Category;
use App\Entity\Blog\Comment;
use App\Entity\Blog\Post;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
  /**
   * @Route("/admin", name="admin")
   * @Route("/dashboard", name="dashboard")
   */
  public function index(): Response
  {
    // return parent::index();
    return $this->render('admin/welcome.html.twig', [
      'dashboard_controller_filepath' => (new \ReflectionClass(
        static::class
      ))->getFileName(),
      'welcome' => $this->renderView('@EasyAdmin/welcome.html.twig', [
        'dashboard_controller_filepath' => (new \ReflectionClass(
          static::class
        ))->getFileName(),
      ]),
    ]);
  }

  public function configureDashboard(): Dashboard
  {
    return Dashboard::new()
      ->setTitle('Auth')
      // the path defined in this method is passed to the Twig asset() function
      ->setFaviconPath('favicon.svg')

      // the domain used by default is 'messages'
      ->setTranslationDomain('my-auth-domain')

      // there's no need to define the "text direction" explicitly because
      // its default value is inferred dynamically from the user locale
      ->setTextDirection('ltr')

      // set this option if you prefer the page content to span the entire
      // browser width, instead of the default design which sets a max width
      ->renderContentMaximized()

      // set this option if you prefer the sidebar (which contains the main menu)
      // to be displayed as a narrow column instead of the default expanded design
      // ->renderSidebarMinimized()

      // by default, all backend URLs include a signature hash. If a user changes any
      // query parameter (to "hack" the backend) the signature won't match and EasyAdmin
      // triggers an error. If this causes any issue in your backend, call this method
      // to disable this feature and remove all URL signature checks
      ->disableUrlSignatures()

      // by default, all backend URLs are generated as absolute URLs. If you
      // need to generate relative URLs instead, call this method
      ->generateRelativeUrls();
  }

  public function configureMenuItems(): iterable
  {
    yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
    // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);

    yield MenuItem::section('Blog');
    yield MenuItem::linkToCrud('Categories', 'fa fa-tags', Category::class)
      // ->setController(LegacyCategoryCrudController::class),
      ->setDefaultSort(['createdAt' => 'DESC']);
    // links to a different CRUD action
    yield MenuItem::linkToCrud(
      'Add Category',
      'fa fa-tags',
      Category::class
    )->setAction('new');

    yield MenuItem::linkToCrud('Blog Posts', 'fa fa-file-text', Post::class)
      ->setDefaultSort(['createdAt' => 'DESC']);
    yield MenuItem::linkToCrud(
      'Add Post',
      'fa fa-tags',
      Post::class
    )->setAction('new');

    yield MenuItem::section('Users');
    yield MenuItem::linkToCrud('Comments', 'fa fa-comment', Comment::class)
      ->setDefaultSort(['createdAt' => 'DESC']);
    yield MenuItem::linkToCrud(
      'Add Comment',
      'fa fa-tags',
      Comment::class
    )->setAction('new');
    yield MenuItem::linkToCrud('Users', 'fa fa-user', User::class)
      ->setDefaultSort(['createdAt' => 'DESC']);
    yield MenuItem::linkToCrud(
      'Add User',
      'fa fa-tags',
      User::class
    )->setAction('new');
  }
}
