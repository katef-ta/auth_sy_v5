<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * @ IsGranted("ROLE_ADMIN")
 */
class NavigationController extends AbstractController
{
    /**
     * @Route("/navigation", name="navigation")
     */
    public function index(): Response
    {
        return $this->render('navigation/index.html.twig', [
            'controller_name' => 'NavigationController',
        ]);
    }

    /**
     * @Route("/", name="home")
     */
    public function home(SessionInterface $session)
    {
        $data = [];

        if ($session->has('message')) {
            $message = $session->get('message');
            $session->remove('message'); //on vide la variable message dans la session
            $data['message'] = $message; //on ajoute à l'array de paramètres notre message
        }

        return $this->render('navigation/home.html.twig', @compact('data'));
    }

    /**
     * @Route("/membre", name="membre")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function membre(SessionInterface $session)
    {
        //test si un utilisateur est connecté
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $data = [];

        if ($session->has('message')) {
            $message = $session->get('message');
            $session->remove('message'); //on vide la variable message dans la session
            $data['message'] = $message; //on ajoute à l'array de paramètres notre message
        }

        return $this->render('navigation/membre.html.twig', @compact('data'));
    }

    /**
     * @Route("/admin", name="admin")
     * @IsGranted("ROLE_ADMIN")
     */
    public function admin(SessionInterface $session)
    {
        //récupération de l'utilisateur security>Bundle
        $utilisateur = $this->getUser();

        if (!$utilisateur) {
            $session->set('message', 'Merci de vous connecter');
            return $this->redirectToRoute('app_login');
        }

        //vérification des droits.
        if ($utilisateur && in_array(User::ROLE_ADMIN, $utilisateur->getRoles())) {
            return $this->render('navigation/admin.html.twig');
        }

        //redirection
        $session->set(
            'message',
            "Vous n'avez pas le droit d'acceder à la page admin vous avez été redirigé sur cette page"
        );
        return $this->redirectToRoute('home');
    }
}
