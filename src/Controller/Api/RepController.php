<?php

namespace App\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Faker\Factory;

class RepController extends AbstractController
{
  /**
   * @Route("/reps", name="reps_index", methods={"GET","HEAD"})
   */
  public function index(Request $request): JsonResponse
  {
    if($request->isMethod(Request::METHOD_POST)) {
      return $this->add($request);
    }

    $data = [
      'items' => [],
    ];

    $faker = Factory::create('fr_FR');

    foreach (range(1, $data['limit'] = $request->get('limit', 10)) as $index) {
      $data['items'][$index] = [
        'id' => $faker->uuid(),
        'itemLabel' => $faker->name,
        'reps' => $faker->numberBetween(0, 200),
        'totalWeightLifted' => $faker->numberBetween(20, 80),
        'date' => $faker
          ->dateTime()
          ->add(new \DateInterval(sprintf("PT%sH", $faker->randomNumber(2)))),
      ];
    }

    return new JsonResponse($data);
  }

  /**
   * @Route("/reps/add", name="reps_add", methods={"GET", "POST","HEAD"})
   */
  public function add(Request $request): JsonResponse
  {
    $data = [];

    return new JsonResponse($data);
  }

  /**
   * @Route("/reps/{id}", name="reps_view", methods={"GET","HEAD", "DELETE"})
   */
  public function delete(Request $request, string $id): JsonResponse
  {
      $data = [];

      if($id) {
        $data['id'] = $id;
      }

      if($request->isMethod(Request::METHOD_DELETE)) {
        $data['action'] = strtolower(Request::METHOD_DELETE);
      }

      return new JsonResponse($data);
  }
}
