<?php

declare(strict_types=1);

namespace App\Controller\Chat;

use App\Entity\Message;
use App\Repository\ConversationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use App\Entity\GroupConversation;
use App\Entity\User;
use App\Form\MessageType;
use App\Repository\MessageRepository;
use App\Repository\UserRepository;
use App\Service\CookieGenerator;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Uid\Uuid;

class MessageController extends AbstractController
{

    /**
     * @var MessageRepository
     */
    private $messageRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Security
     */
    private Security $security;

    private ParameterBagInterface $parameterBag;

    public function __construct(ParameterBagInterface $parameterBag, MessageRepository $messageRepository,
                                UserRepository $userRepository,
                                EntityManagerInterface $em,
                                Security $security)
    {
        $this->parameterBag = $parameterBag;
        $this->messageRepository    = $messageRepository;
        $this->userRepository       = $userRepository;
        $this->em                   = $em;
        $this->security             = $security;
    }

    /**
     * @Route("/message", name="message", methods={"POST"})
     */
    public function sendMessage(
        Request $request,
        ConversationRepository $conversationRepository,
        SerializerInterface $serializer,
        EntityManagerInterface $em,
        HubInterface $publisher
    ): JsonResponse {
        $data = \json_decode($request->getContent(), true);
        if (empty(($content = $data['content']))) {
            throw new AccessDeniedHttpException('No data sent');
        }

        $conversation = $conversationRepository->findOneBy([
            'id' => $data['conversation'],
        ]);
        if (!$conversation) {
            throw new EntityNotFoundException('Message have to be sent on a specific conversation');
        }

        $message = new Message();
        $message->setContent($content);
        $message->setConversation($conversation);
        $message->setAuthor($this->getUser());

        $em->persist($message);
        $em->flush();

        $jsonMessage = $serializer->serialize($message, 'json', [
            'groups' => ['message'],
        ]);

        $update = new Update(
            sprintf('%s/conversation/%s', $this->parameterBag->get('mercure_host'), $conversation->getId()),
            $jsonMessage,
            true
        );
        $publisher($update);

        return new JsonResponse($jsonMessage, Response::HTTP_OK, [], true);
    }

    /**
     * @Route("/message/{id}", name="get_message", methods={"GET"})
     */
    public function getMessage(Message $message, SerializerInterface $serializer): JsonResponse
    {
        return new JsonResponse(
            $serializer->serialize($message, 'json', [
                'groups' => ['message'],
            ]),
            Response::HTTP_OK,
            [],
            true
        );
    }

//BREAD controller action pattern

    /**
     * Display list of messages from conversation
     *
     * @Route("/messages/{groupConversation}", name="messages_browse")
     * @param GroupConversation $groupConversation
     * @return Response
     */
    public function browse(GroupConversation $groupConversation): Response {

        $this->denyAccessUnlessGranted('ROLE_USER');

        $messages = $this->messageRepository->findMessageByConversationId($groupConversation->getId());

        $response = $this->render('message/browse.html.twig', [
            'conversation' => $groupConversation,
            'messages' => $messages,
        ]);

        return $response;
    }

    /**
     * Create new message
     *
     * @Route("/messages/{id}/add", name="messages_add", requirements={"id" : "\d+"})
     */
    public function add(Request $request,
                        HubInterface $hub,
                        GroupConversation $groupConversation): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        /**
         * @var User $user
         * used with connected user
         */
        $user = $this->security->getUser();
        if(!($user)) {
            $this->addFlash('error', 'Utilisateur incorrect.');
            return $this->redirectToRoute('app_login');
        }

        $message = new Message();

        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);
        $content = $request->get('message-box', null);

        if ($content) {

            $message->setCreatedAt(new \DateTime('now'));
            // $message->setUpdated(new \DateTime('now'));
            $message->setContent($content);
            $message->setMine(true);
            $message->setSeen(false);

            $message->setAuthor($user);
            $groupConversation->addMessage($message);

            try {
                $date   = new \DateTime('now');
                $update = new Update(
                    '/messages/' . $groupConversation->getId(), //IRI, the topic being updated, can be any string usually URL
                    json_encode([
                        'conversation'  => 'Nouveau message conversation :' . $groupConversation->getName(),
                        'message'       => $content,
                        'from'          => $user->getEmail(),
                        'to'            => $groupConversation->getUsers(),
                        'date'          => $date->format('H:i'),
                    ]), //the content of the update, can be anything
                    $groupConversation->getPrivate(), //private
                    'message-' . Uuid::v4(),//mercure id
                'message'
                );

            //PUBLISHER JWT : doit contenir la liste des conversations dans lesquels il peut publier conf => mercure.publish
            //SUBSCRIBER JWT: doit contenir la liste des conversations dans lesquels il peut recevoir conf => mercure.subcribe

                $hub->publish($update);
                $this->em->flush();
            }
            catch (\Exception $e) {
                //dd($groupConversation);
                throw $e;
            }
        }

        return $this->redirectToRoute('messages_browse', ['groupConversation' => $groupConversation->getId()] );
    }

    /**
     * Ping mercure
     * @Route("/messages/{id}/ping", name="messages_ping")
     */
    public function ping(Request $request, HubInterface $hub)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $update = new Update(
            '/ping/' .  $request->get('id'), //IRI, the topic being updated, can be any string usually URL
            json_encode(['message' => 'pinged !']), //the content of the update, can be anything
            false, //private
            'ping-' . Uuid::v4(), //mercure id
            'ping'
        );

        $hub->publish($update);

        return $this->redirectToRoute('messages_browse', ['groupConversation' => $request->get('id')]);
    }
}
