<?php

declare(strict_types=1);

namespace App\Controller\Chat;

use App\Entity\Conversation;
use App\Repository\ConversationRepository;
use App\Repository\MessageRepository;
use App\Services\Mercure\CookieGenerator;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\QueryBuilder;
use Omines\DataTablesBundle\Adapter\Doctrine\Event\ORMAdapterQueryEvent;
use Omines\DataTablesBundle\Adapter\Doctrine\ORM\SearchCriteriaProvider;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapterEvents;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\DataTableFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\WebLink\Link;

class ConversationController extends AbstractController
{
    private ParameterBagInterface $parameterBag;

    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
    }

    /**
     * @Route("/conversation/index", name="conversation_index")
     */
    public function index(Request $request, DataTableFactory $dataTableFactory): Response
    {
        $table = $dataTableFactory
            ->create()
            ->add('id', TextColumn::class)
            ->add('name', TextColumn::class)
            ->add('messages', TextColumn::class, [
                'render' => function ($value, $context) {
                    return $value instanceof Collection ? $value->count() : 0;
                },
            ])
            // ->add('messages', TextColumn::class)
            // ->createAdapter(ArrayAdapter::class, [
            //     ['firstName' => 'Donald', 'lastName' => 'Trump'],
            //     ['firstName' => 'Barack', 'lastName' => 'Obama'],
            // ])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Conversation::class,
                'query' => function (QueryBuilder $builder) {
                    $builder
                        ->select('c')
                        ->addSelect('m')
                        ->addSelect('a')
                        ->from(Conversation::class, 'c')
                        ->leftJoin('c.messages', 'm')
                        ->leftJoin('m.author', 'a');
                },
                /*'criteria' => [
                    function (QueryBuilder $builder) {
                        $builder->andWhere($builder->expr()->like('c.name', ':test'))->setParameter('test', '%ny 2%');
                    },
                    new SearchCriteriaProvider(),
                ],*/
            ]);

        $table->addEventListener(ORMAdapterEvents::PRE_QUERY, function (ORMAdapterQueryEvent $event) {
            $event
                ->getQuery()
                ->enableResultCache()
                ->useQueryCache(true);
        });

        $table->handleRequest($request);

        if ($table->isCallback()) {
            return $table->getResponse();
        }

        if (
            $this->getParameter('kernel.environment') == 'dev' &&
            $this->getParameter('kernel.debug') &&
            function_exists('dump')
        ):
            dump(@compact('table'));
        endif;

        return $this->render('chat/conversation/index.html.twig', ['datatable' => $table]);
    }

    /**
     * @Route("/chat", name="chat_home")
     */
    public function getConversations(ConversationRepository $conversationRepository): Response
    {
        $conversations = $conversationRepository->findAll();

        $response = $this->render('chat/index.html.twig', [
            'conversations' => $conversations,
        ]);
        $response->headers->set('Link', [
            sprintf('<%s:3000/.well-known/mercure>', $this->parameterBag->get('mercure_host')),
            'rel="mercure"',
        ]);

        return $response;
    }

    /**
     * @Route("/chat/{id}", name="chat_conversation")
     */
    public function chat(
        Request $request,
        Conversation $conversation,
        MessageRepository $messageRepository,
        CookieGenerator $cookieGenerator
    ): Response {
        $messages = $messageRepository->findBy(
            [
                'conversation' => $conversation,
            ],
            ['createdAt' => 'ASC']
        );

        $hubUrl = $this->getParameter('mercure.default_hub');
        $this->addLink($request, new Link('mercure', $hubUrl));

        $response = $this->render('chat/chat.html.twig', [
            'conversation' => $conversation,
            'messages' => $messages,
        ]);
        $response->headers->setCookie(
            Cookie::create(
                'mercureAuthorization',
                $cookieGenerator($conversation),
                new \DateTime('+1day'),
                '/.well-known/mercure'
            )
        );
        return $response;
    }
}
