<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * @ IsGranted("ROLE_ADMIN")
 */
class HomeController extends AbstractController
{
  /**
   * @Route("/index", name="home_index")
   */
  public function index(): Response
  {
    return $this->render('home/index.html.twig', [
      'controller_name' => 'HomeController',
    ]);
  }

  /**
   * @Route("/react", name="react")
   */
  public function react(): Response
  {
    return $this->render('home/react.html.twig', [
      'controller_name' => 'HomeController::react',
    ]);
  }

  /**
   * @Route("/react_admin", name="react_admin")
   */
  public function reactAdmin(): Response
  {
    return $this->render('home/react_admin.html.twig', [
      'controller_name' => 'HomeController::react_admin',
    ]);
  }

  /**
   * @Route("/react_context", name="react_admin")
   */
  public function reactContextAdmin(): Response
  {
    return $this->render('home/react_context.html.twig', [
      'controller_name' => 'HomeController::react_context',
    ]);
  }
}
