<?php

declare(strict_types=1);

namespace App\Services\Mercure;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Token\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Key\InMemory;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mercure\Jwt\TokenProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class JWTProvider implements TokenProviderInterface
{
    private ParameterBagInterface $parameterBag;
    private Configuration $config;
    private string $secret;
    private $tokenStorage;
    private $em;

    /**
     * @var Security
     */
    private Security $security;

    public function __construct(
        ParameterBagInterface $parameterBag,
        EntityManagerInterface $em,
        Security $security,
        TokenStorageInterface $tokenStorage
    ) {
        $this->parameterBag = $parameterBag;
        $this->secret = $this->parameterBag->get('mercure_secret');
        // $this->config = $config;
        $this->config = Configuration::forSymmetricSigner(
            new Sha256(),
            InMemory::plainText($this->secret)
        );
        $this->tokenStorage = $tokenStorage;
        $this->em = $em;
        $this->security = $security;
    }

    public function __invoke(): string
    {
        $now = new \DateTimeImmutable();

        return $this->config
            ->builder()
            ->issuedBy($this->parameterBag->get('mercure_host'))
            ->withHeader('iss', $this->parameterBag->get('mercure_host'))
            ->permittedFor($this->parameterBag->get('mercure_host'))
            // bin2hex(random_bytes(16))
            ->identifiedBy($this->parameterBag->get('mercure_secret'))
            ->issuedAt($now)
            ->canOnlyBeUsedAfter($now->modify('+30 second'))
            ->expiresAt($now->modify('+2 hour'))
            ->withClaim('mercure', ['publish' => ['*']])
            // ->withClaim('uid', 1)
            ->getToken($this->config->signer(), $this->config->signingKey())
            ->toString();
    }

    public function getJWT(): string
    {
        $subscribe = [];

        /** @var User $user */
        $user = $this->security->getUser();

        if ($user) {
            $conversations = $user->getConversations()->getValues();
            //save all sub/pub conversations
            if ($conversations) {
                foreach ($conversations as $conversation) {
                    $subscribe[] = '/messages/' . $conversation->getId();
                }
            }

            $subscribe[] = '/ping/{id}';

            $config = Configuration::forSymmetricSigner(new Sha256(), InMemory::plainText($this->secret));
            $token = $config
                ->builder()
                ->withClaim('mercure', [
                    'subscribe' => $subscribe,
                    'publish' => $subscribe,
                ])
                // Builds a new token
                ->getToken($config->signer(), $config->signingKey());

            //dd( $token->toString());
            return $token->toString();
        }
        return "";
    }
}
