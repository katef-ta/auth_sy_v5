<?php

declare(strict_types=1);

namespace App\Services\Mercure;

use App\Entity\Conversation;
use Lcobucci\JWT\Token\Builder;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Token;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Cookie;

use function bin2hex;
use function random_bytes;

class CookieGenerator
{
    private Configuration $config;
    private string $secret;
    private ParameterBagInterface $parameterBag;
    private string $jwt;

    public function __construct(ParameterBagInterface $parameterBag, JWTProvider $JWTProvider)
    {
        $this->parameterBag = $parameterBag;
        $this->secret = $this->parameterBag->get('mercure_secret');
        // $this->config = $config;
        $this->config = Configuration::forSymmetricSigner(new Sha256(), InMemory::plainText($this->secret));
        //On load grace à l'autowiring le service qui génére le JWT token
        $this->jwt = $JWTProvider->getJWT();
    }

    public function __invoke(Conversation $conversation): string
    {
        return $this->config
            ->builder()
            // bin2hex(random_bytes(16))
            ->identifiedBy($this->parameterBag->get('mercure_secret'))
            ->getToken($this->config->signer(), $this->config->signingKey())
            ->toString();
    }

    public function generate(): Cookie
    {
        $domain = $_ENV["APP_ENV"] == "dev" ? 'localhost' : '.monsite.com';
        $secure = $_ENV["APP_ENV"] == "dev" ? false : true;
        $httpOnly = false;

        //        $config = Configuration::forSymmetricSigner(new Sha256(), InMemory::plainText($this->secret));
        //
        //        $token  = $config->builder()
        //            // Configures the issuer (iss claim)
        //            //->issuedBy('https://flameup.com')
        //            // Configures the audience (aud claim)
        //            //->permittedFor('https://app.flameup.com')
        //            // Configures the id (jti claim)
        //            //->identifiedBy('4f1g23a12aa')
        //            // Configures the time that the token was issue (iat claim)
        //            //->issuedAt($now)
        //            // Configures the time that the token can be used (nbf claim)
        //            //->canOnlyBeUsedAfter($now->modify('+1 minute'))
        //            // Configures the expiration time of the token (exp claim)
        //            //->expiresAt($now->modify('+3 hour'))
        //            // Configures a new claim, called "uid"
        //            //@TODO :  gerer les chanels de souscription / publication
        //            ->withClaim('mercure', [
        //                'subscribe' => ['*'],
        //                'publish' => ['*']
        //            ])
        //            // Configures a new header, called "foo"
        //            //->withHeader('Access-Control-Allow-Origin', '*')
        //            // Builds a new token
        //            ->getToken($config->signer(), $config->signingKey());
        //
        //        //compagre JWT generation
        //dd($this->jwt);

        return Cookie::create(
            'mercureAuthorization',
            $this->jwt,
            0,
            '/.well-known/mercure',
            $domain,
            $secure,
            $httpOnly,
            false,
            'lax'
        );
    }
}
