module.exports = {
    extends: ['eslint:recommended'],
    parserOptions: {
        ecmaVersion: 6,
        sourceType: 'module',
        ecmaFeatures: {
            jsx: true
        }
    },
    env: {
        browser: true,
        es6: true,
        node: true
    },
    rules: {
        "no-console": 0,
        "no-unused-vars": 0,
        // "semi": ["error", "always"],
        // "quotes": ["error", "double"],
        // "max-len": ["error", { "code": 80, "tabWidth": 4 }],
    }
};