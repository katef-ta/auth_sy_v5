<?php

use PHPUnit\Framework\TestCase;
use App\Utils\FileLoader;

class FileLoaderTest extends TestCase
{
    public function testFileLoaderClassCanBeCreated()
    {
        $f = new FileLoader();
    }

    /**
     * Nous voulons récupérer le contenu d'un fichier via
     * une méthode get()
     */
    public function testFileLoaderCanLoadFileContent()
    {
        $f = new FileLoader();
        $r = $f->get(__DIR__ . '/fixtures/simple.md');
        $this->assertEquals("Foo\n", $r);
    }
}
