<?php

declare(strict_types=1);

namespace App\Tests\Unit\Repository;

use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserRepositoryTest extends KernelTestCase
{
  private ManagerRegistry $doctrine;
  private UserPasswordHasherInterface $passwordHasher;

  // use FixturesTrait;

  // ...

  public function testFindAdminUser(): void
  {
    // $this->loadDb();

    $adminUser = $this->doctrine
      ->getRepository(User::class)
      ->findOneBy(['username' => 'admin']);

    $this->assertSame('admin@noreply.local', $adminUser->getEmail());
    $this->assertEquals(
      $this->passwordHasher->hashPassword($adminUser, 'test1234'),
      $adminUser->getPassword()
    );
    $this->assertSame('ROLE_ADMIN', $adminUser->getRole()->getCode());
    $this->assertEquals(
      new \DateTime() instanceof \DateTime,
      $adminUser->getCreatedAt() instanceof \DateTime
    );

    $this->tearDown();
  }

  // ...
}
