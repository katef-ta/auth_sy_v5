-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 31 mars 2022 à 11:11
-- Version du serveur : 5.7.36
-- Version de PHP : 7.4.28

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Base de données : `auth_sy5`
--
CREATE DATABASE IF NOT EXISTS `auth_sy5` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `auth_sy5`;

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_64C19C1727ACA70` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `category`
--

INSERT INTO `category` (`id`, `name`, `created_at`, `updated_at`, `parent_id`) VALUES
(1, 'IT', '2022-03-15 08:30:10', '2022-03-15 08:30:10', NULL),
(2, 'IA', '2022-03-15 08:30:10', '2022-03-15 08:30:10', NULL),
(3, 'cc', '2022-02-28 02:22:00', '2022-03-11 04:42:00', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `channel`
--

DROP TABLE IF EXISTS `channel`;
CREATE TABLE IF NOT EXISTS `channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `channel`
--

INSERT INTO `channel` (`id`, `name`) VALUES
(1, 'react'),
(2, 'users'),
(3, 'members');

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) DEFAULT NULL,
  `subject` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `approved` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `post_id` int(11) NOT NULL,
  `published_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_9474526CFBCE3E7A` (`subject`),
  KEY `IDX_9474526CF675F31B` (`author_id`),
  KEY `IDX_9474526C4B89032C` (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `comment`
--

INSERT INTO `comment` (`id`, `author_id`, `subject`, `content`, `approved`, `created_at`, `updated_at`, `post_id`, `published_at`) VALUES
(1, 1, 'lamda', 'lamda', 1, '2022-03-09 04:04:00', '2022-03-18 03:33:00', 3, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20220307162059', '2022-03-07 16:35:15', 277);

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

DROP TABLE IF EXISTS `message`;
CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B6BD307F72F5A1AA` (`channel_id`),
  KEY `IDX_B6BD307FF675F31B` (`author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `page`
--

DROP TABLE IF EXISTS `page`;
CREATE TABLE IF NOT EXISTS `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_140AB6202B36786B` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `post`
--

DROP TABLE IF EXISTS `post`;
CREATE TABLE IF NOT EXISTS `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) DEFAULT NULL,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `status` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `published_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_5A8A6C8D2B36786B` (`title`),
  KEY `IDX_5A8A6C8DF675F31B` (`author_id`),
  KEY `IDX_5A8A6C8D12469DE2` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `post`
--

INSERT INTO `post` (`id`, `author_id`, `title`, `content`, `status`, `created_at`, `updated_at`, `category_id`, `slug`, `summary`, `published_at`) VALUES
(2, 1, 'lorem ipsum', 'lorem ipsum', 'published', '2022-03-15 08:29:32', '2022-03-15 08:29:32', 1, '', '', NULL),
(3, NULL, 'alpha', 'beta tetha', 'dezd', '2022-03-15 08:29:32', '2022-03-15 08:29:32', 1, '', '', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `post_tag`
--

DROP TABLE IF EXISTS `post_tag`;
CREATE TABLE IF NOT EXISTS `post_tag` (
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`post_id`,`tag_id`),
  KEY `IDX_5ACE3AF04B89032C` (`post_id`),
  KEY `IDX_5ACE3AF0BAD26311` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D34A04AD12469DE2` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `tag`
--

DROP TABLE IF EXISTS `tag`;
CREATE TABLE IF NOT EXISTS `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_389B7835E237E06` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`, `created_at`, `updated_at`) VALUES
(1, 'alpha@gmail.com', '[\"ROLE_ADMIN\"]', '$2y$13$m2/v/7WCyOgmut9yralLdOSG4HW/YKFxzT19QKneDh1gaclbz6qWK', '2022-03-08 16:26:54', '2022-03-23 10:38:12'),
(2, 'beta@gmail.com', '[\"ROLE_ADMIN\"]', '$2y$13$m2/v/7WCyOgmut9yralLdOSG4HW/YKFxzT19QKneDh1gaclbz6qWK', '2022-03-08 16:26:54', '2022-03-23 10:38:12'),
(4, 'theta@gmail.com', '[\"ROLE_ADMIN\"]', '$2y$13$m2/v/7WCyOgmut9yralLdOSG4HW/YKFxzT19QKneDh1gaclbz6qWK', '2022-03-08 16:26:54', '2022-03-23 10:38:13'),
(5, 'gamma@gmail.com', '[\"ROLE_USER\"]', '$2y$13$m2/v/7WCyOgmut9yralLdOSG4HW/YKFxzT19QKneDh1gaclbz6qWK', '2022-03-08 16:26:54', '2022-03-23 10:38:13'),
(6, 'lamda@gmail.com', '[\"ROLE_USER\"]', '$2y$13$m2/v/7WCyOgmut9yralLdOSG4HW/YKFxzT19QKneDh1gaclbz6qWK', '2022-03-18 13:46:17', '2022-03-23 10:38:14'),
(9, 'phi@gmail.com', '[\"ROLE_USER\"]', '$2y$13$m2/v/7WCyOgmut9yralLdOSG4HW/YKFxzT19QKneDh1gaclbz6qWK', '2022-03-23 10:17:43', '2022-03-23 10:38:14'),
(10, 'mi@gmail.com', '[\"ROLE_USER\"]', '$2y$13$m2/v/7WCyOgmut9yralLdOSG4HW/YKFxzT19QKneDh1gaclbz6qWK', '2022-03-23 10:18:55', '2022-03-23 10:38:15'),
(12, 'pi@gmail.com', '[\"ROLE_USER\"]', '$2y$13$m2/v/7WCyOgmut9yralLdOSG4HW/YKFxzT19QKneDh1gaclbz6qWK', '2022-03-23 10:21:06', '2022-03-23 10:38:16'),
(13, 'po@gmail.com', '[\"ROLE_USER\"]', '$2y$13$m2/v/7WCyOgmut9yralLdOSG4HW/YKFxzT19QKneDh1gaclbz6qWK', '2022-03-23 10:22:39', '2022-03-23 10:38:16'),
(14, 'ko@gmail.com', '[\"ROLE_USER\"]', '$2y$13$m2/v/7WCyOgmut9yralLdOSG4HW/YKFxzT19QKneDh1gaclbz6qWK', '2022-03-23 10:35:41', '2022-03-23 10:38:17'),
(15, 'mo@gmail.com', '[\"ROLE_USER\"]', '$2y$13$m2/v/7WCyOgmut9yralLdOSG4HW/YKFxzT19QKneDh1gaclbz6qWK', '2022-03-23 10:35:46', '2022-03-23 10:38:17'),
(16, 'go@gmail.com', '[\"ROLE_USER\", \"ROLE_ADMIN\"]', '$2y$13$m2/v/7WCyOgmut9yralLdOSG4HW/YKFxzT19QKneDh1gaclbz6qWK', '2022-03-23 10:38:08', '2022-03-23 10:38:18'),
(17, 'vi@gmail.com', '[]', '$2y$13$WfJqo62atAhbJ66qNtmZweSsmuO/EXOdLw6Yc7JMMLZ3PPIktl7hS', '2022-03-31 08:50:41', '2022-03-31 10:39:12');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `FK_64C19C1727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `category` (`id`);

--
-- Contraintes pour la table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `FK_9474526C4B89032C` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`),
  ADD CONSTRAINT `FK_9474526CF675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `FK_B6BD307F72F5A1AA` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`),
  ADD CONSTRAINT `FK_B6BD307FF675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `FK_5A8A6C8D12469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `FK_5A8A6C8DF675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `post_tag`
--
ALTER TABLE `post_tag`
  ADD CONSTRAINT `FK_5ACE3AF04B89032C` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_5ACE3AF0BAD26311` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `FK_D34A04AD12469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);
SET FOREIGN_KEY_CHECKS=1;
COMMIT;
