<?php

declare(strict_types=1);

namespace App\Tests;

use App\Tests\Helpers\PasswordSameAssertion;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

uses(KernelTestCase::class, PasswordSameAssertion::class)->in('Unit');

uses(KernelTestCase::class)->in('Helpers');

beforeEach(function () {
    // ...
});

test('verify admin email is valid', fn () =>
	expect($this->userAdmin->getEmail())->toBe('admin@noreply.local'));

test('verify the admin user password', fn () =>
	$this->assertSamePassword($this->userAdmin, 'test1234'));

test('verify user admin role', fn () =>
	expect($this->userAdmin->getRole()->getCode())->toBe('ROLE_ADMIN'));

test('verify createdat field is a DateTime', fn () =>
	expect($this->userAdmin->getCreatedAt()->toBeInstanceOf(\DateTime::class)));

    // expect()->extend('toBeWithinRange', function ($min, $max) {
    //     return $this->toBeGreaterThanOrEqual($min)
    //                 ->toBeLessThanOrEqual($max);
    // });
