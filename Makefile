# Fix color output until TravisCI fixes https://github.com/travis-ci/travis-ci/issues/7967
export FORCE_COLOR = true

SOURCES = packages codemods eslint

COMMA := ,
EMPTY :=
SPACE := $(EMPTY) $(EMPTY)
COMMA_SEPARATED_SOURCES = $(subst $(SPACE),$(COMMA),$(SOURCES))

PHP := php
COMPOSER := composer
PHPBINCONSOLE := $(PHP) bin/console
YARN := yarn
NODE := $(YARN) node
RUNBAT := run.bat
RUNSH := run.sh

PROD_ENV := APP_ENV=prod BABEL_ENV=prod
DEV_ENV := APP_ENV=dev BABEL_ENV=development

SHELL := bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

FIG			:= DOCKER_BUILDKIT=1 COMPOSE_DOCKER_CLI_BUILD=1 docker-compose
PHP_EXEC	:= $(FIG) exec web
SYMFONY	 	:= $(PHP) bin/console

.PHONY: build build-dist watch lint fix clean test-clean test-only test test-ci publish bootstrap

composer-install:
	$(PHP) $(COMPOSER) install

composer-update:
	$(PHP) $(COMPOSER) update

doctrine-schema-update:
	$(PHPBINCONSOLE) d:s:u --dump-sql --force

doctrine-update: doctrine-schema-update

clear-cache-dev:
	$(PHPBINCONSOLE) c:c -e dev

clear-cache-prod:
	$(PHPBINCONSOLE) c:c -e prod

clear-cache-test:
	$(PHPBINCONSOLE) c:c -e test

clear-cache: clear-cache-dev clear-cache-prod clear-cache-test

yarn-install:
	$(YARN)

yarn-build:
	$(DEV_ENV) $(YARN) dev

yarn-watch:
	$(DEV_ENV) $(YARN) dev --watch

run:
	$(RUNSH)
	$(RUNBAT)

phpstan:
	$(PHP) vendor/bin/phpstan analyse src

phpstan-test:
	$(PHP) vendor/bin/phpstan analyse src tests

lint-yaml:
	$(PHP) bin/console lint:yaml config translations

lint-twig:
	$(PHP) bin/console lint:twig templates

lint: lint-yaml lint-twig

# SOURCE : https://github.com/babel/babel/blob/main/Makefile

rector-init:
	$(PHP) vendor/bin/rector init

rector:
	$(PHP) vendor/bin/rector process src --dry-run --debug --xdebug

rector-test:
	$(PHP) vendor/bin/rector process src tests --dry-run --debug --xdebug

build:
	cp .env.local.dist .env.local
	$(FIG) build

start:
	$(FIG) up -d

kill:
	$(FIG) kill
	$(FIG) rm -f

db: vendor/autoload.php
	$(SYMFONY) do:da:dr --force --if-exists
	$(SYMFONY) do:da:cr
	$(SYMFONY) do:mi:mi --no-interaction --allow-no-migration
	$(SYMFONY) do:fi:lo --no-interaction

vendor/autoload.php: composer.lock
	$(PHP) composer install
